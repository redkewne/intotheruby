
class Rover
  
  attr_accessor :pos, :facing, :storage

  def initialize
    @facing = 'n'
    @storage = []
    @pos = [0,0]
  end

  def move(dir)
    # movement dirs are mapped to direction rover may be facing.
    # directions rover may be facing are mapped to [desired_axis, change]

    mapping = {'f' => {'n' => [1,1], 'e' => [0,1], 's' => [1,-1], 'w' => [0,-1]},
	       'b' => {'n' => [1,-1], 'e' => [0,-1], 's' => [1,1], 'w' => [0,1]}}
    
    if mapping.include? dir    
      info = mapping[dir][@facing]
      @pos[info[0]] += info[1]

    end
  end
   

  def rotate(dir)
    mapping = {'r' => {'n' => 'e', 'e' => 's', 's' => 'w', 'w' => 'n'},
	   'l' => {'n' => 'w', 'w' => 's', 's' => 'e', 'e' => 'n'}}

    if mapping.include? dir
      @facing = mapping[dir][@facing]
    end
  end
 
  def commands(cmds)
	  
    for cmd in cmds do
      if cmd[0] == 'm'
        self.move(cmd[1])
      elsif cmd[0] == 'r'
        self.rotate(cmd[1])
      else
      end
    end
  end 
end

