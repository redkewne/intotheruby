require 'rspec/autorun'
require './rover.rb'


describe Rover do
	before(:each) do @r = Rover.new end
  
  it "inits with [0,0] pos, [] storage, facing 'n'" do
    expect(@r.pos).to match_array([0,0])
    expect(@r.storage).to be_empty
    expect(@r.facing).to eq('n')
  end

  it "moves forward taking into account its direction" do
    @r.move('f')	  
    expect(@r.pos).to match_array([0,1])

    @r.facing = 's'
    @r.move('f')	  
    expect(@r.pos).to match_array([0,0])

    @r.facing = 'e'
    @r.move('f')	  
    expect(@r.pos).to match_array([1,0])

    @r.facing = 'w'
    @r.move('f')	  
    expect(@r.pos).to match_array([0,0])


  end

  it "moves backwards taking into account its direction" do
    current_y = @r.pos[1]
    @r.move('b')
    expect(@r.pos[1]).to eq(current_y - 1)

    current_y = @r.pos[1]
    @r.facing = 's'
    @r.move('b')
    expect(@r.pos[1]).to eq(current_y + 1)

    current_x = @r.pos[0]
    @r.facing = 'e'
    @r.move('b')
    expect(@r.pos[0]).to eq(current_x - 1)

    current_x = @r.pos[0]
    @r.facing = 'w'
    @r.move('b')
    expect(@r.pos[0]).to eq(current_y + 1)

  end
      
  it "rotates +90 degrees" do
    @r.rotate('r')
    expect(@r.facing).to eq('e')
    
    @r.rotate('r')
    expect(@r.facing).to eq('s')
    
    @r.rotate('r')
    expect(@r.facing).to eq('w')
    
    @r.rotate('r')
    expect(@r.facing).to eq('n')
  end

  it "rotates -90 degrees" do
    @r.rotate('l')
    expect(@r.facing).to eq('w')
    
    @r.rotate('l')
    expect(@r.facing).to eq('s')
    
    @r.rotate('l')
    expect(@r.facing).to eq('e')
    
    @r.rotate('l')
    expect(@r.facing).to eq('n')
  end

  it "accepts and processes a list of commands" do
    @r.commands(['rt', 'ww', 'cd'])
    expect(@r.pos).to match_array([0,0])

    @r.commands(['mf', 'rl', 'mb', 'mb', 'rr', 'mf', 'mf'])
    expect(@r.pos).to match_array([2,3])
    
    @r.pos = [0,0]
    @r.commands(['mf', 'mf', 'mf', 'rr', 'mf', 'mf', 'mf', 'rr', 'mb'])
    expect(@r.pos).to match_array([3, 4])

  end

end

