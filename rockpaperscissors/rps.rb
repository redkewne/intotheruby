
# Rock, Paper, Scissors game built with Ruby for learning purposes.

CHOICES = ['r', 'p', 's']

TRIANGLE = {"r" => "s", "s" => "p", "p" => "r"}

p1score = 0
p2score = 0

def make_selection
	print 'Please select either "r", "p", or "s":'
	choice = gets.chomp

	if CHOICES.include? choice
		return choice
	else
		puts 'Wrong input.'
		make_selection
	end
    end

until p1score == 3 or p2score == 3
	
	p1hand = make_selection
	p2hand = CHOICES.sample
	
	puts "You chose #{p1hand}, opponent chose #{p2hand}"
	
	if p1hand == p2hand
		puts "Tie!"
	
	elsif p1hand == TRIANGLE[p2hand]
		puts "You lose!"
		p2score += 1
	else
		puts "You win!"
		p1score += 1
	
	end

end

puts "Game over."
p1score > p2score ? puts{"You win with #{p1score} points!"} : puts {"You lose with #{p1score} points!"}

